use http;
use std::io::{Read, Write};
use std::net::{TcpListener, TcpStream};
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(about = "A simple little reverse proxy.")]
struct Config {
    /// The local port to listen for requests on.
    listening_port: u16,
    /// The local port to forward requests to.
    forwarding_port: u16,
}

fn main() {
    let config = Config::from_args();

    // Open and bind a socket to listen for requests on.
    let listen_sock = TcpListener::bind(("localhost", config.listening_port)).expect(&format!(
        "Could not bind to listening port {}",
        config.listening_port
    ));

    // Open and connect a socket to forward requests to.
    let mut forward_sock =
        TcpStream::connect(("localhost", config.forwarding_port)).expect(&format!(
            "Could not connect to remote port {} for forwarding",
            config.forwarding_port
        ));

    let mut incoming_socks: Vec<TcpStream> = vec![];

    // Accept connections.
    match listen_sock.accept() {
        Ok((conn_sock, addr)) => {
            println!(
                "Accepted connection on port {} from client at {}",
                config.listening_port, addr
            );
            incoming_socks.push(conn_sock);
        }
        Err(e) => println!("Could not accept connection: {}", e),
    }

    loop {
        // Receive new data.
        let mut incoming_buffer: [u8; 4096] = [0; 4096];
        for mut sock in &incoming_socks {
            match sock.read(&mut incoming_buffer) {
                Ok(read_len) => {
                    println!(
                        "Got {} bytes of data from client at {}: {:?}",
                        read_len,
                        &sock.peer_addr().unwrap(),
                        &incoming_buffer[0..read_len]
                    );
                    // Pass on data.
                    match forward_sock.write(&incoming_buffer[0..read_len]) {
                        Ok(write_len) => {
                            println!(
                                "Passed on {} bytes of data from client at {}",
                                write_len,
                                &sock.peer_addr().unwrap(),
                            );
                        }
                        Err(e) => println!(
                            "Error while passing on data from client at {}: {}",
                            &sock.peer_addr().unwrap(),
                            e
                        ),
                    }
                }
                Err(e) => println!(
                    "Error while reading data from client at {}: {}",
                    &sock.peer_addr().unwrap(),
                    e
                ),
            }
        }
    }
}
